# Description

The jupyter notebooks presented on this repository show the workflow with respect to my master's thesis "Automated Knowledge Discovery". For my analysis I have used PATSTAT data which requires authorised access so i presented the required SQL-scripts to obtain the data and create the tables I have used in the appendix of the thesis. Using those queries and adhering to the scheme used in my thesis one should easily be able to replicate my analysis.

# Content

- First Use Case - Overall Regional data
- Second Use Case - European Patents concerned with material science and chemicals
- Third Use Case - European Patents concerend with microelectronics and telecommunications

# Use case Description

The first use case provides the validation of the regional knowledge base for the regions of Styria, Upper Austria and Carinthia. To this end, we perform our topical clustering approach on the entire regional research corpus to see if the dominant areas are adequately captured in distinct thematic clusters.
The second use case will be the filtering of one higher level IPC class into its smaller subclasses. The area of material science and chemicals, especially polymers, will be used to outline this approach.
The third use case is the evaluation of clusters based on information provided by experts. We will examine the area of electronics and telecommunications, especially microelectronics, to see if we get similar keywords and distinct clusters from a more generic selection of the overall population of suitable patent applications.



